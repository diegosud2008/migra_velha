
// import ExcelReader from './ExcelRead';
import React, { Component } from 'react';
import Firebase from "firebase";
import "firebase/firebase-storage";
import ExcelRead from './ExcelRead';
let json = require('./imobBrasil2.json');
// const planilha = require('imoveis_paulo.xlsx');
class App extends Component{
  state = {
    img: ''
  }
  img = (img) => {
    console.log(img);
  }
  async componentDidMount(){
    let config = {
      apiKey: "AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk",
      authDomain: "smartimob-dev-test.firebaseapp.com",
      databaseURL: "https://smartimob-dev-test.firebaseio.com",
      projectId: "smartimob-dev-test",
      storageBucket: "smartimob-dev-test.appspot.com",
      messagingSenderId: "1030189605462",
      appId: "1:1030189605462:web:cb42f70fe0f892048051f7",
      measurementId: "G-KC82QWH0ZD"
    }
    Firebase.initializeApp(config);
    let imovel = [];
    let imoCompleto = {};
    let i = 0;
    let imoveis = await Firebase.firestore().collection(`empresas/w5pxFTB5mE0oAkExKEmB/imoveis`).limit(1).get();
    imoveis = imoveis.docs.map( imo => ({...imo.data()}))
    let img = []
    let results = [];
    
    imoveis[0].fotos.map( (imgs,index) => {
        fetch(`${imgs.source.uri}`).then(function(response) {
            return response.blob();
            })
            .then(function(myBlob) {
            var objectURL = URL.createObjectURL(myBlob);
            var reader = new FileReader();
            var storageRef = Firebase.storage().ref();
            reader.readAsDataURL(myBlob);
            reader.onload = async (e) => {
                var fotoRef = storageRef.child(`empresas/bF5kuD7M3HCzZSLTHlRw/${imoveis[0].db_id}/${index}_${index}.jpeg`);
                var fotoRefmini = storageRef.child(`empresas/bF5kuD7M3HCzZSLTHlRw/${imoveis[0].db_id}/${index}_${index}_mini.jpeg`);
                let snapshot = await fotoRef.putString(e.target.result, 'data_url')
                let snapshotmini = await fotoRefmini.putString(e.target.result, 'data_url');
                
                results.push({
                    destaque: e === 0 ? true : false,
                    height: null,
                    id: `${index}`,
                    uploaded: true,
                    width: null,
                    resized: await fotoRef.getDownloadURL(),
                    source:{uri: await fotoRefmini.getDownloadURL()}
                })
                Firebase.firestore().doc(`empresas/bF5kuD7M3HCzZSLTHlRw/imoveis/${imoveis[0].db_id}`).update({fotos: results})
            };
        });
        
      
    })
    //   
    //   setTimeout(async () => {
    //     // var storageRef = Firebase.storage().ref();
    //     let results = [];
    //     img.map( (image,index) => {
    //         var fotoRef = storageRef.child(`empresas/bF5kuD7M3HCzZSLTHlRw/${image.imovel}/${index}_${index}.jpeg`);
    //         var fotoRefmini = storageRef.child(`empresas/bF5kuD7M3HCzZSLTHlRw/${image.imovel}/${index}_${index}_mini.jpeg`);
    //         let snapshot = await fotoRef.putString(image.foto, 'data_url')
    //         let snapshotmini = await fotoRefmini.putString(image.foto, 'data_url');
    //         results.push({
    //             destaque: e === 0 ? true : false,
    //             height: null,
    //             id: `${imo.pkimovel}_${e}`,
    //             uploaded: true,
    //             width: null,
    //             resized: await fotoRef.getDownloadURL(),
    //             source:{uri: await fotoRefmini.getDownloadURL()}
    //           })
    //     })
    //     img
    //     //     var fotoRef = storageRef.child(`empresas/bF5kuD7M3HCzZSLTHlRw/10/10_10.jpeg`);
    //     //     var fotoRefmini = storageRef.child(`empresas/bF5kuD7M3HCzZSLTHlRw/10/10_10_mini.jpeg`);
    //     //     let snapshot = await fotoRef.putString(localStorage.foto, 'data_url')
    //     //     let snapshotmini = await fotoRefmini.putString(localStorage.foto, 'data_url');
    //     //     console.log(await fotoRefmini.getDownloadURL())
    //   },2000)
      
    //   for await (const foto of Object.keys(fotos)) {
    //     if (fotos[foto].file.name) {
            
    //         results.push({
    //             destaque: fotos[foto].destaque || false,
    //             height: null,
    //             id: `${id_imovel}_${index}`,
    //             uploaded: true,
    //             width: null,
    //             resized: await fotoRefmini.getDownloadURL(),
    //             source:{uri: await fotoRef.getDownloadURL()}
    //         })
    //         total = total+quantidade;
    //         dispatch({
    //             type: TOTAL,
    //             quantidade: total,
    //             fotos: true
    //         })
    //     index++
    //     }
    // }
    //   let imoveis = await Firebase.firestore().collection(`empresas/bF5kuD7M3HCzZSLTHlRw/imoveis`).where('codigo','==',imoCompleto[id].ref).get();4
    // })
    // let imoveis = Firebase.firestore().collection(`empresas/bF5kuD7M3HCzZSLTHlRw/imoveis`).where('agenciador_id','==',null).get();
    // imoveis = await (await imoveis).docs.map( async dado => {
    //   await Firebase.firestore().doc(`empresas/bF5kuD7M3HCzZSLTHlRw/imoveis/${dado.id}`).update({excluido: true})
    //   console.log(dado.id)
    // })
    // console.log(imoveis)
  }
  render(){
    return(
      <div>
        <ExcelRead/>
      </div>
    )
  }
}


export default App;
