
// import ExcelReader from './ExcelRead';
import React, { Component } from 'react';
import Firebase from "firebase";
import "firebase/firebase-storage";
import ExcelRead from './ExcelRead';
let imoveis = require('./i9/imoveis.json');
let fotos = require('./i9/fotos.json');
let clientes = require('./i9/clientes.json');
let atendimentos = require('./i9/atendimento.json');
// global.atob = Base64.encode;
// const planilha = require('imoveis_paulo.xlsx');
class App extends Component{
  state = {
    img: [],
    imoveis: "",
  }
  img = (img) => {
    console.log(img);
  }
  async componentDidMount(){
    let config = {
      apiKey: "AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk",
      authDomain: "smartimob-dev-test.firebaseapp.com",
      databaseURL: "https://smartimob-dev-test.firebaseio.com",
      projectId: "smartimob-dev-test",
      storageBucket: "smartimob-dev-test.appspot.com",
      messagingSenderId: "1030189605462",
      appId: "1:1030189605462:web:cb42f70fe0f892048051f7",
      measurementId: "G-KC82QWH0ZD"
    }
    Firebase.initializeApp(config);
    let imovel = [];
    let imoCompleto = {};
    let i = 0;
    let proprietarios = [];
    let caracteristicas= [
        {
            id: 'Closet',
            nome: 'Closet',
        },
        {
            id: 'Cozinha',
            nome: 'Cozinha',
        },
        {
            id: 'Armário Dormitório',
            nome: 'Armário Dormitório',
        },
        {
            id: 'Armário no Banheiro',
            nome: 'Armário no Banheiro',
        },
        {
            id: 'Casa fria',
            nome: 'Cama Fria',
        },
        {
            id: 'Casa Caseiro',
            nome: 'Casa Caseiro',
        },
        {
            id: 'Churrasqueira',
            nome: 'Churrasqueira',
        },
        {
            id: 'Dispensa',
            nome: 'Dispensa',
        },
        {
            id: 'Energia',
            nome: 'Energia',
        },
        {
            id: 'Escritorio',
            nome: 'Escritorio',
        },
        {
            id: 'Esgoto',
            nome: 'Esgoto',
        },
        {
            id: 'Gás Natural',
            nome: 'Gás Natural',
        },
        {
            id: 'Lavanderia',
            nome: 'Lavanderia',
        },
        {
            id: 'Pavimentação',
            nome: 'Pavimentação',
        },
        {
            id: 'Piscina',
            nome: 'Piscina',
        },
        {
            id: 'Piso Frio',
            nome: 'Piso Frio',
        },
        {
            id: 'Portão Eletronico',
            nome: 'Portão Eletronico',
        },
        {
            id: 'Portaria 24h',
            nome: 'Portaria 24h',
        },
        {
            id: 'Recepição',
            nome: 'Recepição',
        },
        {
            id: 'Sacada',
            nome: 'Sacada',
        },
        {
            id: 'Cerca',
            nome: 'Cerca',
        },
        {
            id: 'Varanda',
            nome: 'Varanda',
        },
        {
            id: 'Murada',
            nome: 'Murada',
        },
        {
            id: 'Mesanino',
            nome: 'Mesanino',
        },
        {
            id: 'Deposito',
            nome: 'Deposito',
        },
        {
            id: 'Ar Condicionado',
            nome: 'Ar Condicionado',
        },
        {
            id: 'Aceita financiamento',
            nome: 'Aceita financiamento',
        },
        {
            id: 'Financiado',
            nome: 'Financiado',
        },
        {
            id: 'Condomínio',
            nome: 'Condomínio',
        },
        {
            id: 'Aquecedor central',
            nome: 'Aquecedor central',
        },
        {
            id: 'Alarme',
            nome: 'Alarme',
        },
        {
            id: 'Campo de futbol',
            nome: 'Campo de futbol',
        },
        {
            id: 'Cozinha Americana',
            nome: 'Cozinha Americana',
        },
        {
            id: 'Copa',
            nome: 'Copa',
        },
        {
            id: 'Decorado',
            nome: 'Decorado',
        },
        {
            id: 'Edicula',
            nome: 'Edicula',
        },
        {
            id: 'Elevador de serviço',
            nome: 'Elevador de serviço',
        },
        {
            id: 'Entrada de Serviço',
            nome: 'Entrada de Serviço',
        },
        {
            id: 'Hidro',
            nome: 'Hidro',
        },
        {
            id: 'Jardim',
            nome: 'Jardim',
        },
        {
            id: 'Lareira',
            nome: 'Lareira',
        },
        {
            id: 'Mobiliado',
            nome: 'Mobiliado',
        },
        {
            id: 'Piscina aquecida',
            nome: 'Piscina aquecida',
        },
        {
            id: 'Quintal',
            nome: 'Quintal',
        },
        {
            id: 'Sala de ginastica',
            nome: 'Sala de ginastica',
        },
        {
            id: 'Sala de festa',
            nome: 'Sala de festa',
        },
        {
            id: 'Sala de jogo',
            nome: 'Sala de jogo',
        },
        {
            id: 'Telefone',
            nome: 'Telefone',
        },
        {
            id: 'Poço',
            nome: 'Poço',
        },
        {
            id: 'Hidrometro',
            nome: 'Hidrometro',
        },
        {
            id: 'Sacada',
            nome: 'Sacada',
        },
        {
            id: 'Perciana',
            nome: 'Perciana',
        },
        {
            id: 'Água quente',
            nome: 'Água quente',
        },
        {
            id: 'Deck',
            nome: 'Deck',
        },
        {
            id: 'Engradeado',
            nome: 'Engradeado',
        },
        {
            id: 'Hall',
            nome: 'Hall',
        },
        {
            id: 'Semi-mobiliado',
            nome: 'Semi-mobiliado',
        },
        {
            id: 'Imóveis Planejados',
            nome: 'Imóveis Planejados',
        },
        {
            id: 'Split',
            nome: 'Split',
        },
        {
            id: 'Espera Split',
            nome: 'Espera Split',
        },
        {
            id: 'Terraço',
            nome: 'Terraço',
        },
        {
            id: 'Vista pro Mar',
            nome: 'Vista pro Mar',
        },
        {
            id: 'Vista pra Serra',
            nome: 'Vista pra Serra',
        },
        {
            id: 'Vista pra Cidade',
            nome: 'Vista pra Cidade',
        },
        {
            id: 'Sacada Fundo',
            nome: 'Sacada Fundo',
        },
        {
            id: 'Porcelanato',
            nome: 'Porcelanato',
        },
        {
            id: 'PlayGround',
            nome: 'PlayGround',
        },
        {
            id: 'Laminado',
            nome: 'Laminado',
        },
        {
            id: 'Taco',
            nome: 'Taco',
        },
        {
            id: 'Varanda Gurmer',
            nome: 'Varanda Gurmer',
        },
        {
            id: 'Piso Ceramica',
            nome: 'Piso Ceramica',
        },
        {
            id: 'Guarita',
            nome: 'Guarita',
        },
        {
            id: 'Area de Lazer',
            nome: 'Area de Lazer',
        },
        {
            id: 'Aquecedor Solar',
            nome: 'Aquecedor Solar',
        },
        {
            id: 'Interfone',
            nome: 'Interfone',
        },
        {
            id: 'Gourmet',
            nome: 'Gourmet',
        }
    ]
    let campos_personalizados_values = [
        {
            nome: 'Água',
            ref: 'Água',
            created_at: new Date(),
            tipo: 'bool',
        },
        {
            ref: 'Andares',
            created_at: new Date(),
            nome: 'Andares',
            tipo: 'text',
        },
        {
            ref: 'Apartamento por Andar',
            created_at: new Date(),
            nome: 'Apartamento por Andar',
            tipo: 'text',
        },
        {
            ref: 'Elevador',
            created_at: new Date(),
            nome: 'Elevador',
            tipo: 'text',
        },
        {
            ref: 'Sala',
            created_at: new Date(),
            nome: 'Sala',
            tipo: 'text',
        },
        {
            ref: 'Ocupação',
            created_at: new Date(),
            nome: "Ocupação",
            tipo: "text",
        },
        {
            ref: 'Ano Construção',
            created_at: new Date(),
            nome: "Ano Construção",
            tipo: "text",
        },
        {
            ref: 'Ponto de Referencia',
            created_at: new Date(),
            nome: "Ponto de Referencia",
            tipo: "text",
        },
        {
            ref: 'Sala de Jantar',
            created_at: new Date(),
            nome: "Sala de Jantar",
            tipo: "text",
        },
        {
            ref: 'Sala de TV',
            created_at: new Date(),
            nome: "Sala de TV",
            tipo: "text",
        },
        {
            ref: 'Localização',
            created_at: new Date(),
            nome: "Localização",
            tipo: "text",
        }
    ]
    let bairros_disponiveis = [];
    let bairro = [];
    let tipo_disponiveis = [];
    let tipos = [];
    let paramers =  new URLSearchParams(window.location.search)
    let index =  parseInt(paramers.get('atualiza')) || 0;
    let imove = [];
    console.log(imoveis)
    
    for await(let imo of imoveis) {
        imove.push(imo)
    }
    setTimeout( async () => {
        console.log(imove)
        if (index !== imove.length){
            let template = {
                caracteristicas: [],
                banheiros: '',
                preço_condominio: 0,
                fotos: [],
                fotos_thumbnail: [],
                CEP_confirmado: true,
                CEP_error: false,
                excluido: false,
                agenciador_id: "",
                codigo: '',
                db_id: '',
                aut_venda: [],
                aut_visita: [],
                cadastrador_id: "",
                campos_personalizados_values: [],
                created_at: new Date(),
                proprietario_id: '',
                bairro: '',
                rua: '',
                número: '',
                estado: {
                id: '',
                nome: '',
                value: ''
                },
                CEP: '',
                cidade: {
                id: '',
                nome: '',
                value: ''
                },
                exibirEndereco: true,
                vagas: '',
                suítes: '',
                dormitórios: '',
                tipo: '',
                aceita_permuta: false,
                mostrar_permuta_no_site: false,
                venda_exibir_valor_no_site: true,
                permuta_desc: "",
                area_total: '',
                area_construída: "",
                area_privativa: "",
                area_útil: "",
                preço_venda: '',
                preço_venda_desconto: 0,
                preço_locação: 0,
                preço_locação_desconto: 0,
                venda_autorizacao: false,
                venda_financiamento: false,
                locação_exibir_valor_no_site: false,
                locação_autorizacao: false,
                bloqueio_fotos: false,
                locação: false,
                venda: true,
                IPTU: 0,
                titulo: "",
                descrição: '',
                desc_interna: "",
                video_youtube: "",
                palavras_chaves: "",
                link_tour_virtual: "",
                inativo: false,
                destaque: ""
            }
            let imo = imove[index]
            this.setState({
                imoveis: imo.pkimovel
            })
            console.log(imo)
        //   if (index <= 2) {
            let foto = fotos.filter( foto => foto.fkimovel === imo.pkimovel);
            console.log(foto.length)
            let imgs = []
            let results = [];
            let e = 0;
            for await( let img of foto ) {
              let im = await fetch(`http://localhost/test_sockets.php?imagem=${img.url}`).then( dados => dados.json());
              imgs.push(im.caminho);
              var storageRef = Firebase.storage().ref();
                var fotoRef = storageRef.child(`empresas/pDYLY5rnbnmKxJGQJNku/${imo.pkimovel}/${imo.pkimovel}_${e}.jpeg`);
                let snapshot = await fotoRef.putString(im.caminho, 'data_url')
                results.push({
                  destaque: e === 0 ? true : false,
                  height: null,
                  id: `${imo.pkimovel}_${e}`,
                  uploaded: true,
                  width: null,
                  resized: await fotoRef.getDownloadURL(),
                  source:{uri: await fotoRef.getDownloadURL()}
                })
                // if (e === this.state.img.length) {
                    
                    e++;
            } 
          let cidade;
          if (imo.principalcep)
              cidade = await fetch(`https://cep.awesomeapi.com.br/json/${imo.principalcep}`).then(dado => dado.json());
    
          template = {
              ...template,
              db_id: imo.pkimovel,
              proprietario_id: imo.fkcodcli && imo.fkcodcli !== "0" ? imo.fkcodcli : "",
              campos_personalizados_values: [
                  {
                      nome: 'Água',
                      tipo: 'bool',
                      value: imo.detalheagua === 1 ? true : false
                  },
                  {
                      nome: 'Andares',
                      tipo: 'text',
                      value: imo.detalheandares || ""
                  },
                  {
                      nome: 'Apartamento por Andar',
                      tipo: 'text',
                      value: imo.detalheaptoandar || ""
                  },
                  {
                      nome: 'Elevador',
                      tipo: 'text',
                      value: imo.detalheelevador || ""
                  },
                  {
                      nome: 'Sala',
                      tipo: 'text',
                      value: imo.detalhesala || ""
                  },
                  {
                      nome: "Ocupação",
                      tipo: "text",
                      value: imo.principalocupacao || ""
                  },
                  {
                      nome: "Ano Construção",
                      tipo: "text",
                      value: imo.localplacalocalal ? imo.localplacalocalal : ""
                  },
                  {
                      nome: "Ponto de Referencia",
                      tipo: "text",
                      value: imo.principalpontoref ? imo.principalpontoref : ""
                  },
                  {
                      nome: "Sala de Jantar",
                      tipo: "text",
                      value: imo.principalsalajantar ? imo.principalsalajantar : ""
                  },
                  {
                      nome: "Sala de TV",
                      tipo: "text",
                      value: imo.principalsalatv ? imo.principalsalatv : ""
                  },
                  {
                      nome: "Localização",
                      tipo: "text",
                      value: imo.principallocalizacao ? imo.principallocalizacao : ""
                  }
              ],
              empreendimento_id: imo.codempre || 0,
              area_total: imo.detalheareaterreno || 0 ,
              area_útil: imo.detalheareautil || 0,
              area_construída: imo.detalheareaconst || 0,
              caracteristicas: [
                  {
                      id: 'Closet',
                      nome: 'Closet',
                      value: imo.detalhearmcloset === "1" ? true :false
                  },
                  {
                      id: 'Cozinha',
                      nome: 'Cozinha',
                      value: imo.detalhecozinha === "1" ? true :false
                  },
                  {
                      id: 'Armário Dormitório',
                      nome: 'Armário Dormitório',
                      value: imo.detalhearmdormitorio === "1" ? true :false
                  },
                  {
                      id: 'Armário no Banheiro',
                      nome: 'Armário no Banheiro',
                      value: imo.detalhearmbanheiro === "1" ? true :false
                  },
                  {
                      id: 'Casa fria',
                      nome: 'Cama Fria',
                      value: imo.detalhecamarafria === "1" ? true :false
                  },
                  {
                      id: 'Casa Caseiro',
                      nome: 'Casa Caseiro',
                      value: imo.detalhecasacaseiro === "1" ? true :false
                  },
                  {
                      id: 'Churrasqueira',
                      nome: 'Churrasqueira',
                      value: imo.detalhechurrasqueira === "1" ? true :false
                  },
                  {
                      id: 'Dispensa',
                      nome: 'Dispensa',
                      value: imo.detalhedespensa === "1" ? true :false
                  },
                  {
                      id: 'Energia',
                      nome: 'Energia',
                      value: imo.detalheenergia === "1" ? true :false
                  },
                  {
                      id: 'Escritorio',
                      nome: 'Escritorio',
                      value: imo.detalheescritorio === "1" ? true :false
                  },
                  {
                      id: 'Esgoto',
                      nome: 'Esgoto',
                      value: imo.detalheesgoto === "1" ? true :false
                  },
                  {
                      id: 'Gás Natural',
                      nome: 'Gás Natural',
                      value: imo.detalhegasnatural === "1" ? true :false
                  },
                  {
                      id: 'Lavanderia',
                      nome: 'Lavanderia',
                      value: imo.detalhelavanderia === "1" ? true :false
                  },
                  {
                      id: 'Pavimentação',
                      nome: 'Pavimentação',
                      value: imo.detalhepavimentacao === "1" ? true :false
                  },
                  {
                      id: 'Piscina',
                      nome: 'Piscina',
                      value: imo.detalhepiscina === "1" ? true :false
                  },
                  {
                      id: 'Piso Frio',
                      nome: 'Piso Frio',
                      value: imo.detalhepisofrio === "1" ? true :false
                  },
                  {
                      id: 'Portão Eletronico',
                      nome: 'Portão Eletronico',
                      value: imo.detalheportaoeletronico === "1" ? true :false
                  },
                  {
                      id: 'Portaria 24h',
                      nome: 'Portaria 24h',
                      value: imo.detalheportaria24h === "1" ? true :false
                  },
                  {
                      id: 'Recepição',
                      nome: 'Recepição',
                      value: imo.detalherecepcao === "1" ? true :false
                  },
                  {
                      id: 'Sacada',
                      nome: 'Sacada',
                      value: imo.detalhesacada === "1" ? true :false
                  },
                  {
                      id: 'Cerca',
                      nome: 'Cerca',
                      value: imo.detalhecerca === "1" ? true :false
                  },
                  {
                      id: 'Varanda',
                      nome: 'Varanda',
                      value: imo.detalhevaranda === "1" ? true :false
                  },
                  {
                      id: 'Murada',
                      nome: 'Murada',
                      value: imo.detalhemurado === "1" ? true :false
                  },
                  {
                      id: 'Mesanino',
                      nome: 'Mesanino',
                      value: imo.detalhemezanino === "1" ? true :false
                  },
                  {
                      id: 'Deposito',
                      nome: 'Deposito',
                      value: imo.detalhedeposito === "1" ? true :false
                  },
                  {
                      id: 'Ar Condicionado',
                      nome: 'Ar Condicionado',
                      value: imo.detalhearcondicionado === "1" ? true :false
                  },
                  {
                      id: 'Aceita financiamento',
                      nome: 'Aceita financiamento',
                      value: imo.principalaceitaf === "1" ? true :false
                  },
                  {
                      id: 'Financiado',
                      nome: 'Financiado',
                      value: imo.principalfinanciado === "1" ? true :false
                  },
                  {
                      id: 'Condomínio',
                      nome: 'Condomínio',
                      value: imo.principalcondominio === "1" ? true :false
                  },
                  {
                      id: 'Aquecedor central',
                      nome: 'Aquecedor central',
                      value: imo.detalheaquececentral === "1" ? true :false
                  },
                  {
                      id: 'Alarme',
                      nome: 'Alarme',
                      value: imo.detalhealarme === "1" ? true :false
                  },
                  {
                      id: 'Campo de futbol',
                      nome: 'Campo de futbol',
                      value: imo.detalhecampfutebol === "1" ? true :false
                  },
                  {
                      id: 'Cozinha Americana',
                      nome: 'Cozinha Americana',
                      value: imo.detalhecozamericana === "1" ? true :false
                  },
                  {
                      id: 'Copa',
                      nome: 'Copa',
                      value: imo.detalhecopa === "1" ? true :false
                  },
                  {
                      id: 'Decorado',
                      nome: 'Decorado',
                      value: imo.detalhedecorado === "1" ? true :false
                  },
                  {
                      id: 'Edicula',
                      nome: 'Edicula',
                      value: imo.detalheedicula === "1" ? true :false
                  },
                  {
                      id: 'Elevador de serviço',
                      nome: 'Elevador de serviço',
                      value: imo.detalheelevadorservico === "1" ? true :false
                  },
                  {
                      id: 'Entrada de Serviço',
                      nome: 'Entrada de Serviço',
                      value: imo.detalheentservico === "1" ? true :false
                  },
                  {
                      id: 'Hidro',
                      nome: 'Hidro',
                      value: imo.detalhehidro === "1" ? true :false
                  },
                  {
                      id: 'Jardim',
                      nome: 'Jardim',
                      value: imo.detalhejardim === "1" ? true :false
                  },
                  {
                      id: 'Lareira',
                      nome: 'Lareira',
                      value: imo.detalhelareira === "1" ? true :false
                  },
                  {
                      id: 'Mobiliado',
                      nome: 'Mobiliado',
                      value: imo.detalhemobiliado === "1" ? true :false
                  },
                  {
                      id: 'Piscina aquecida',
                      nome: 'Piscina aquecida',
                      value: imo.detalhepiscaquecida === "1" ? true :false
                  },
                  {
                      id: 'Quintal',
                      nome: 'Quintal',
                      value: imo.detalhequintal === "1" ? true :false
                  },
                  {
                      id: 'Sala de ginastica',
                      nome: 'Sala de ginastica',
                      value: imo.detalhesalaginastica === "1" ? true :false
                  },
                  {
                      id: 'Sala de festa',
                      nome: 'Sala de festa',
                      value: imo.detalhesalafesta === "1" ? true :false
                  },
                  {
                      id: 'Sala de jogo',
                      nome: 'Sala de jogo',
                      value: imo.detalhesalajogo === "1" ? true :false
                  },
                  {
                      id: 'Telefone',
                      nome: 'Telefone',
                      value: imo.detalhetelefone === "1" ? true :false
                  },
                  {
                      id: 'Poço',
                      nome: 'Poço',
                      value: imo.detalhepoco === "1" ? true :false
                  },
                  {
                      id: 'Hidrometro',
                      nome: 'Hidrometro',
                      value: imo.detalhehidrometro === "1" ? true :false
                  },
                  {
                      id: 'Sacada',
                      nome: 'Sacada',
                      value: imo.detalhesacadalat === "1" ? true :false
                  },
                  {
                      id: 'Perciana',
                      nome: 'Perciana',
                      value: imo.detalhepersianaelet === "1" ? true :false
                  },
                  {
                      id: 'Água quente',
                      nome: 'Água quente',
                      value: imo.detalheaguaquente === "1" ? true :false
                  },
                  {
                      id: 'Deck',
                      nome: 'Deck',
                      value: imo.detalhedeck === "1" ? true :false
                  },
                  {
                      id: 'Engradeado',
                      nome: 'Engradeado',
                      value: imo.detalhegradeado === "1" ? true :false
                  },
                  {
                      id: 'Hall',
                      nome: 'Hall',
                      value: imo.detalhehall === "1" ? true :false
                  },
                  {
                      id: 'Semi-mobiliado',
                      nome: 'Semi-mobiliado',
                      value: imo.detalhesemimobiliado === "1" ? true :false
                  },
                  {
                      id: 'Imóveis Planejados',
                      nome: 'Imóveis Planejados',
                      value: imo.detalhemoveisplanej === "1" ? true :false
                  },
                  {
                      id: 'Split',
                      nome: 'Split',
                      value: imo.detalhesplit === "1" ? true :false
                  },
                  {
                      id: 'Espera Split',
                      nome: 'Espera Split',
                      value: imo.detalheesperasplit === "1" ? true :false
                  },
                  {
                      id: 'Terraço',
                      nome: 'Terraço',
                      value: imo.detalheterraco === "1" ? true :false
                  },
                  {
                      id: 'Vista pro Mar',
                      nome: 'Vista pro Mar',
                      value: imo.detalhevistamar === "1" ? true :false
                  },
                  {
                      id: 'Vista pra Serra',
                      nome: 'Vista pra Serra',
                      value: imo.detalhevistaserra === "1" ? true :false
                  },
                  {
                      id: 'Vista pra Cidade',
                      nome: 'Vista pra Cidade',
                      value: imo.detalhevistacidade === "1" ? true :false
                  },
                  {
                      id: 'Sacada Fundo',
                      nome: 'Sacada Fundo',
                      value: imo.detalhesacadafundo === "1" ? true :false
                  },
                  {
                      id: 'Porcelanato',
                      nome: 'Porcelanato',
                      value: imo.detalheporcelanato === "1" ? true :false
                  },
                  {
                      id: 'PlayGround',
                      nome: 'PlayGround',
                      value: imo.detalheplayground === "1" ? true :false
                  },
                  {
                      id: 'Laminado',
                      nome: 'Laminado',
                      value: imo.detalhelaminado === "1" ? true :false
                  },
                  {
                      id: 'Taco',
                      nome: 'Taco',
                      value: imo.detalhetaco === "1" ? true :false
                  },
                  {
                      id: 'Varanda Gurmer',
                      nome: 'Varanda Gurmer',
                      value: imo.detalhevarandagourmet === "1" ? true :false
                  },
                  {
                      id: 'Piso Ceramica',
                      nome: 'Piso Ceramica',
                      value: imo.detalhepisoceramica === "1" ? true :false
                  },
                  {
                      id: 'Guarita',
                      nome: 'Guarita',
                      value: imo.detalheguarita === "1" ? true :false
                  },
                  {
                      id: 'Area de Lazer',
                      nome: 'Area de Lazer',
                      value: imo.detalhearealazer === "1" ? true :false
                  },
                  {
                      id: 'Aquecedor Solar',
                      nome: 'Aquecedor Solar',
                      value: imo.detalheaquesolar === "1" ? true :false
                  },
                  {
                      id: 'Interfone',
                      nome: 'Interfone',
                      value: imo.detalheinterfone === "1" ? true :false
                  },
                  {
                      id: 'Gourmet',
                      nome: 'Gourmet',
                      value: imo.detalhegourmet === "1" ? true :false
                  }
              ],
              vagas: imo.detalhegaragens || 0,
              banheiros: imo.detalhebanheiros || "",
              dormitórios: imo.detalhedormitorios || "",
              suítes: imo.detalhesuite || "",
              codigo: imo.principalreferencia || 0,
              venda: true,
              locação: imo.principallocalacao === "1" ? true : false,
              aceita_permuta: imo.principalpermuta === "1" ? true : false,
              tipo: imo.principaltipo.toLowerCase().replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }),
              preço_venda: imo.principalvalvenda || 0,
              preço_locação: imo.principalvallocalacao || 0,
              rua: imo.principalendereco || "",
              número: imo.principalnumero || "",
              bairro: imo.principalbairro || "",
              cidade: {
                  id: cidade && cidade.city_ibge? cidade.city_ibge : "",
                  nome: cidade && cidade.city_ibge ? cidade.address : imo.principalcidade ? imo.principalcidade : "",
                  value: cidade && cidade.city_ibge? cidade.city_ibge : ""
              },
              estado: {
                  id: imo.principaluf === "RS" ? 43 : imo.principaluf === "SC" ? 42 : imo.principaluf === "RJ" ? 33 : "",
                  nome: imo.principaluf || "", 
                  value: imo.principaluf === "RS" ? 43 : imo.principaluf === "SC" ? 42 : imo.principaluf === "RJ" ? 33 : ""
              },
              CEP: imo.principalcep || "",
              excluido: imo.principalsituacao === "VENDIDO" || imo.principalsituacao === "INATIVO" ? true : false,
              desc_interna: imo.principaldescricao || "",
              lat: imo.locallatitude || 0,
              long: imo.locallongitude || 0,
              created_at: new Date(imo.captacaocadem),
              edited_at: new Date(imo.captacaoatuaem),
              IPTU: imo.captacaovaliptu || 0,
              preço_condominio: imo.captacaovalcondominio || 0,
              descrição: imo.internetanunciointernet || imo.principaldescricao || "",
              inativo: imo.internetpubsite === "1" ? false : true,
              destaque: imo.internetpubdestaque === "1" ? true : false,
              area_privativa: imo.detalheareaprivativa || 0,
              fotos: results                
          }
          console.log(template)
          let imoveis = await Firebase.firestore().doc(`empresas/pDYLY5rnbnmKxJGQJNku/imoveis/${imo.pkimovel}`).set(template);
          console.log("inseriu: ",imo.fkimovel)  
          window.open(`/?atualiza=${index+1}`,'_self');
        //   }
          index++
          i++;
          return true;
        }
    },5000)
  }
  render(){
    return(
      <div>
        <ExcelRead/>
        <div>
            inserindo o imóvel: {this.state.imoveis}
        </div>
      </div>
    )
  }
}


export default App;
