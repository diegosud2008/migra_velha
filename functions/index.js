
const functions = require('firebase-functions');
let imoveis = require('./pauloAndre/imoveis.json');
let fotos = require('./pauloAndre/fotos.json');
const fetch = require('node-fetch');
// var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const admin = require('firebase-admin');

admin.initializeApp();

exports.insereImo = functions.https.onRequest(async (req, res) => {
    let config = {
        apiKey: "AIzaSyCRY1RsUASByrwHaiXSzgLBa_KjcxA7HDk",
        authDomain: "smartimob-dev-test.firebaseapp.com",
        databaseURL: "https://smartimob-dev-test.firebaseio.com",
        projectId: "smartimob-dev-test",
        storageBucket: "smartimob-dev-test.appspot.com",
        messagingSenderId: "1030189605462",
        appId: "1:1030189605462:web:cb42f70fe0f892048051f7",
        measurementId: "G-KC82QWH0ZD"
      }
    
    // Grab the text parameter.
    const original = req.query.text;
    let i = 0;
    let imgs = []
    imoveis.map( async imo => {
        // if (imo['  Situação'] === 'Disponível') {
        //   // let imoveis = await Firebase.firestore().collection(`empresas/bF5kuD7M3HCzZSLTHlRw/clientes`).where('telefone','==',imo.Celular).get();
        //   // imoveis = await (await imoveis).docs.map( async dado => {
            
        //   // })
        let template = {
          caracteristicas: [],
          banheiros: '',
          preço_condominio: 0,
          fotos: [],
          fotos_thumbnail: [],
          CEP_confirmado: true,
          CEP_error: false,
          excluido: false,
          agenciador_id: "",
          codigo: '',
          db_id: '',
          aut_venda: [],
          aut_visita: [],
          cadastrador_id: "",
          campos_personalizados_values: [],
          created_at: new Date(),
          proprietario_id: '',
          bairro: '',
          rua: '',
          número: '',
          estado: {
          id: '',
          nome: '',
          value: ''
          },
          CEP: '',
          cidade: {
          id: '',
          nome: '',
          value: ''
          },
          exibirEndereco: true,
          vagas: '',
          suítes: '',
          dormitórios: '',
          tipo: '',
          aceita_permuta: false,
          mostrar_permuta_no_site: false,
          venda_exibir_valor_no_site: true,
          permuta_desc: "",
          area_total: '',
          area_construída: "",
          area_privativa: "",
          area_útil: "",
          preço_venda: '',
          preço_venda_desconto: 0,
          preço_locação: 0,
          preço_locação_desconto: 0,
          venda_autorizacao: false,
          venda_financiamento: false,
          locação_exibir_valor_no_site: false,
          locação_autorizacao: false,
          bloqueio_fotos: false,
          locação: false,
          venda: true,
          IPTU: 0,
          titulo: "",
          descrição: '',
          desc_interna: "",
          video_youtube: "",
          palavras_chaves: "",
          link_tour_virtual: "",
          inativo: false,
          destaque: ""
          }
          
            let cidade
          if (imo.principalcep && imo.principalcep !== "0")
              cidade = await fetch(`https://cep.awesomeapi.com.br/json/${imo.principalcep}`).then(dado => dado.json());
          
          
        if (i === 10) {
          let foto = fotos.filter( foto => foto.fkimovel === imo.pkimovel);
          
          foto.map( (img,index) => {
            var request = new XMLHttpRequest();
            request.open('GET', `${img.url}`, true);
            request.setRequestHeader('Accept', '*/*');
            request.responseType = 'blob';
            request.onload = () => {
                var reader = new FileReader();
                reader.readAsDataURL(request.response);
                reader.onload =  (e) => {
                    imgs.push(e.target.result)
                };
            };
            request.send();
          })
          setTimeout( async () => {
            let results = [];
            let e = 0;
            
            imgs.map( async (img,index) => {
              var storageRef = admin.storage().ref();
              var fotoRef = storageRef.child(`empresas/ukMiO7mOb4NdCyEpoSV6/${imo.pkimovel}/${imo.pkimovel}_${index}.jpeg`);
              let snapshot = await fotoRef.putString(img, 'data_url')
              results.push({
                destaque: index === 0 ? true : false,
                height: null,
                id: `${imo.pkimovel}_${index}`,
                uploaded: true,
                width: null,
                resized: await fotoRef.getDownloadURL(),
                source:{uri: await fotoRef.getDownloadURL()}
              })
              e++;
              // if (e === this.state.img.length) {
                  let cidade;
                  if (imo.principalcep)
                      cidade = await fetch(`https://cep.awesomeapi.com.br/json/${imo.principalcep}`).then(dado => dado.json());
                
                  template = {
                      ...template,
                      db_id: imo.pkimovel,
                      proprietario_id: imo.fkcodcli !== "0" ? imo.fkcodcli : "",
                      campos_personalizados_values: [
                          {
                              nome: 'Água',
                              tipo: 'bool',
                              value: imo.detalheagua === 1 ? true : false
                          },
                          {
                              nome: 'Andares',
                              tipo: 'text',
                              value: imo.detalheandares
                          },
                          {
                              nome: 'Apartamento por Andar',
                              tipo: 'text',
                              value: imo.detalheaptoandar
                          },
                          {
                              nome: 'Elevador',
                              tipo: 'text',
                              value: imo.detalheelevador
                          },
                          {
                              nome: 'Sala',
                              tipo: 'text',
                              value: imo.detalhesala
                          },
                          {
                              nome: "Ocupação",
                              tipo: "text",
                              value: imo.principalocupacao
                          },
                          {
                              nome: "Ano Construção",
                              tipo: "text",
                              value: imo.localplacalocalal ? imo.localplacalocalal : ""
                          },
                          {
                              nome: "Ponto de Referencia",
                              tipo: "text",
                              value: imo.principalpontoref ? imo.principalpontoref : ""
                          },
                          {
                              nome: "Sala de Jantar",
                              tipo: "text",
                              value: imo.principalsalajantar ? imo.principalsalajantar : ""
                          },
                          {
                              nome: "Sala de TV",
                              tipo: "text",
                              value: imo.principalsalatv ? imo.principalsalatv : ""
                          },
                          {
                              nome: "Localização",
                              tipo: "text",
                              value: imo.principallocalizacao ? imo.principallocalizacao : ""
                          }
                      ],
                      empreendimento_id: imo.codempre,
                      area_total: imo.detalheareaterreno,
                      area_útil: imo.detalheareautil,
                      area_construída: imo.detalheareaconst,
                      caracteristicas: [
                          {
                              id: 'Closet',
                              nome: 'Closet',
                              value: imo.detalhearmcloset === "1" ? true :false
                          },
                          {
                              id: 'Cozinha',
                              nome: 'Cozinha',
                              value: imo.detalhecozinha === "1" ? true :false
                          },
                          {
                              id: 'Armário Dormitório',
                              nome: 'Armário Dormitório',
                              value: imo.detalhearmdormitorio === "1" ? true :false
                          },
                          {
                              id: 'Armário no Banheiro',
                              nome: 'Armário no Banheiro',
                              value: imo.detalhearmbanheiro === "1" ? true :false
                          },
                          {
                              id: 'Casa fria',
                              nome: 'Cama Fria',
                              value: imo.detalhecamarafria === "1" ? true :false
                          },
                          {
                              id: 'Casa Caseiro',
                              nome: 'Casa Caseiro',
                              value: imo.detalhecasacaseiro === "1" ? true :false
                          },
                          {
                              id: 'Churrasqueira',
                              nome: 'Churrasqueira',
                              value: imo.detalhechurrasqueira === "1" ? true :false
                          },
                          {
                              id: 'Dispensa',
                              nome: 'Dispensa',
                              value: imo.detalhedespensa === "1" ? true :false
                          },
                          {
                              id: 'Energia',
                              nome: 'Energia',
                              value: imo.detalheenergia === "1" ? true :false
                          },
                          {
                              id: 'Escritorio',
                              nome: 'Escritorio',
                              value: imo.detalheescritorio === "1" ? true :false
                          },
                          {
                              id: 'Esgoto',
                              nome: 'Esgoto',
                              value: imo.detalheesgoto === "1" ? true :false
                          },
                          {
                              id: 'Gás Natural',
                              nome: 'Gás Natural',
                              value: imo.detalhegasnatural === "1" ? true :false
                          },
                          {
                              id: 'Lavanderia',
                              nome: 'Lavanderia',
                              value: imo.detalhelavanderia === "1" ? true :false
                          },
                          {
                              id: 'Pavimentação',
                              nome: 'Pavimentação',
                              value: imo.detalhepavimentacao === "1" ? true :false
                          },
                          {
                              id: 'Piscina',
                              nome: 'Piscina',
                              value: imo.detalhepiscina === "1" ? true :false
                          },
                          {
                              id: 'Piso Frio',
                              nome: 'Piso Frio',
                              value: imo.detalhepisofrio === "1" ? true :false
                          },
                          {
                              id: 'Portão Eletronico',
                              nome: 'Portão Eletronico',
                              value: imo.detalheportaoeletronico === "1" ? true :false
                          },
                          {
                              id: 'Portaria 24h',
                              nome: 'Portaria 24h',
                              value: imo.detalheportaria24h === "1" ? true :false
                          },
                          {
                              id: 'Recepição',
                              nome: 'Recepição',
                              value: imo.detalherecepcao === "1" ? true :false
                          },
                          {
                              id: 'Sacada',
                              nome: 'Sacada',
                              value: imo.detalhesacada === "1" ? true :false
                          },
                          {
                              id: 'Cerca',
                              nome: 'Cerca',
                              value: imo.detalhecerca === "1" ? true :false
                          },
                          {
                              id: 'Varanda',
                              nome: 'Varanda',
                              value: imo.detalhevaranda === "1" ? true :false
                          },
                          {
                              id: 'Murada',
                              nome: 'Murada',
                              value: imo.detalhemurado === "1" ? true :false
                          },
                          {
                              id: 'Mesanino',
                              nome: 'Mesanino',
                              value: imo.detalhemezanino === "1" ? true :false
                          },
                          {
                              id: 'Deposito',
                              nome: 'Deposito',
                              value: imo.detalhedeposito === "1" ? true :false
                          },
                          {
                              id: 'Ar Condicionado',
                              nome: 'Ar Condicionado',
                              value: imo.detalhearcondicionado === "1" ? true :false
                          },
                          {
                              id: 'Aceita financiamento',
                              nome: 'Aceita financiamento',
                              value: imo.principalaceitaf === "1" ? true :false
                          },
                          {
                              id: 'Financiado',
                              nome: 'Financiado',
                              value: imo.principalfinanciado === "1" ? true :false
                          },
                          {
                              id: 'Condomínio',
                              nome: 'Condomínio',
                              value: imo.principalcondominio === "1" ? true :false
                          },
                          {
                              id: 'Aquecedor central',
                              nome: 'Aquecedor central',
                              value: imo.detalheaquececentral === "1" ? true :false
                          },
                          {
                              id: 'Alarme',
                              nome: 'Alarme',
                              value: imo.detalhealarme === "1" ? true :false
                          },
                          {
                              id: 'Campo de futbol',
                              nome: 'Campo de futbol',
                              value: imo.detalhecampfutebol === "1" ? true :false
                          },
                          {
                              id: 'Cozinha Americana',
                              nome: 'Cozinha Americana',
                              value: imo.detalhecozamericana === "1" ? true :false
                          },
                          {
                              id: 'Copa',
                              nome: 'Copa',
                              value: imo.detalhecopa === "1" ? true :false
                          },
                          {
                              id: 'Decorado',
                              nome: 'Decorado',
                              value: imo.detalhedecorado === "1" ? true :false
                          },
                          {
                              id: 'Edicula',
                              nome: 'Edicula',
                              value: imo.detalheedicula === "1" ? true :false
                          },
                          {
                              id: 'Elevador de serviço',
                              nome: 'Elevador de serviço',
                              value: imo.detalheelevadorservico === "1" ? true :false
                          },
                          {
                              id: 'Entrada de Serviço',
                              nome: 'Entrada de Serviço',
                              value: imo.detalheentservico === "1" ? true :false
                          },
                          {
                              id: 'Hidro',
                              nome: 'Hidro',
                              value: imo.detalhehidro === "1" ? true :false
                          },
                          {
                              id: 'Jardim',
                              nome: 'Jardim',
                              value: imo.detalhejardim === "1" ? true :false
                          },
                          {
                              id: 'Lareira',
                              nome: 'Lareira',
                              value: imo.detalhelareira === "1" ? true :false
                          },
                          {
                              id: 'Mobiliado',
                              nome: 'Mobiliado',
                              value: imo.detalhemobiliado === "1" ? true :false
                          },
                          {
                              id: 'Piscina aquecida',
                              nome: 'Piscina aquecida',
                              value: imo.detalhepiscaquecida === "1" ? true :false
                          },
                          {
                              id: 'Quintal',
                              nome: 'Quintal',
                              value: imo.detalhequintal === "1" ? true :false
                          },
                          {
                              id: 'Sala de ginastica',
                              nome: 'Sala de ginastica',
                              value: imo.detalhesalaginastica === "1" ? true :false
                          },
                          {
                              id: 'Sala de festa',
                              nome: 'Sala de festa',
                              value: imo.detalhesalafesta === "1" ? true :false
                          },
                          {
                              id: 'Sala de jogo',
                              nome: 'Sala de jogo',
                              value: imo.detalhesalajogo === "1" ? true :false
                          },
                          {
                              id: 'Telefone',
                              nome: 'Telefone',
                              value: imo.detalhetelefone === "1" ? true :false
                          },
                          {
                              id: 'Poço',
                              nome: 'Poço',
                              value: imo.detalhepoco === "1" ? true :false
                          },
                          {
                              id: 'Hidrometro',
                              nome: 'Hidrometro',
                              value: imo.detalhehidrometro === "1" ? true :false
                          },
                          {
                              id: 'Sacada',
                              nome: 'Sacada',
                              value: imo.detalhesacadalat === "1" ? true :false
                          },
                          {
                              id: 'Perciana',
                              nome: 'Perciana',
                              value: imo.detalhepersianaelet === "1" ? true :false
                          },
                          {
                              id: 'Água quente',
                              nome: 'Água quente',
                              value: imo.detalheaguaquente === "1" ? true :false
                          },
                          {
                              id: 'Deck',
                              nome: 'Deck',
                              value: imo.detalhedeck === "1" ? true :false
                          },
                          {
                              id: 'Engradeado',
                              nome: 'Engradeado',
                              value: imo.detalhegradeado === "1" ? true :false
                          },
                          {
                              id: 'Hall',
                              nome: 'Hall',
                              value: imo.detalhehall === "1" ? true :false
                          },
                          {
                              id: 'Semi-mobiliado',
                              nome: 'Semi-mobiliado',
                              value: imo.detalhesemimobiliado === "1" ? true :false
                          },
                          {
                              id: 'Imóveis Planejados',
                              nome: 'Imóveis Planejados',
                              value: imo.detalhemoveisplanej === "1" ? true :false
                          },
                          {
                              id: 'Split',
                              nome: 'Split',
                              value: imo.detalhesplit === "1" ? true :false
                          },
                          {
                              id: 'Espera Split',
                              nome: 'Espera Split',
                              value: imo.detalheesperasplit === "1" ? true :false
                          },
                          {
                              id: 'Terraço',
                              nome: 'Terraço',
                              value: imo.detalheterraco === "1" ? true :false
                          },
                          {
                              id: 'Vista pro Mar',
                              nome: 'Vista pro Mar',
                              value: imo.detalhevistamar === "1" ? true :false
                          },
                          {
                              id: 'Vista pra Serra',
                              nome: 'Vista pra Serra',
                              value: imo.detalhevistaserra === "1" ? true :false
                          },
                          {
                              id: 'Vista pra Cidade',
                              nome: 'Vista pra Cidade',
                              value: imo.detalhevistacidade === "1" ? true :false
                          },
                          {
                              id: 'Sacada Fundo',
                              nome: 'Sacada Fundo',
                              value: imo.detalhesacadafundo === "1" ? true :false
                          },
                          {
                              id: 'Porcelanato',
                              nome: 'Porcelanato',
                              value: imo.detalheporcelanato === "1" ? true :false
                          },
                          {
                              id: 'PlayGround',
                              nome: 'PlayGround',
                              value: imo.detalheplayground === "1" ? true :false
                          },
                          {
                              id: 'Laminado',
                              nome: 'Laminado',
                              value: imo.detalhelaminado === "1" ? true :false
                          },
                          {
                              id: 'Taco',
                              nome: 'Taco',
                              value: imo.detalhetaco === "1" ? true :false
                          },
                          {
                              id: 'Varanda Gurmer',
                              nome: 'Varanda Gurmer',
                              value: imo.detalhevarandagourmet === "1" ? true :false
                          },
                          {
                              id: 'Piso Ceramica',
                              nome: 'Piso Ceramica',
                              value: imo.detalhepisoceramica === "1" ? true :false
                          },
                          {
                              id: 'Guarita',
                              nome: 'Guarita',
                              value: imo.detalheguarita === "1" ? true :false
                          },
                          {
                              id: 'Area de Lazer',
                              nome: 'Area de Lazer',
                              value: imo.detalhearealazer === "1" ? true :false
                          },
                          {
                              id: 'Aquecedor Solar',
                              nome: 'Aquecedor Solar',
                              value: imo.detalheaquesolar === "1" ? true :false
                          },
                          {
                              id: 'Interfone',
                              nome: 'Interfone',
                              value: imo.detalheinterfone === "1" ? true :false
                          },
                          {
                              id: 'Gourmet',
                              nome: 'Gourmet',
                              value: imo.detalhegourmet === "1" ? true :false
                          }
                      ],
                      vagas: imo.detalhegaragens,
                      banheiros: imo.detalhebanheiros,
                      dormitórios: imo.detalhedormitorios,
                      suítes: imo.detalhesuite,
                      codigo: imo.principalreferencia,
                      venda: true,
                      locação: imo.principallocalacao === "1" ? true : false,
                      aceita_permuta: imo.principalpermuta === "1" ? true : false,
                      tipo: imo.principaltipo.toLowerCase().replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }),
                      preço_venda: imo.principalvalvenda,
                      preço_locação: imo.principalvallocalacao,
                      rua: imo.principalendereco,
                      número: imo.principalnumero,
                      bairro: imo.principalbairro,
                      cidade: {
                          id: cidade && cidade.city_ibge,
                          nome: cidade ? cidade.address : imo.principalcidade,
                          value: cidade && cidade.city_ibge
                      },
                      estado: {
                          id: imo.principaluf === "RS" ? 43 : imo.principaluf === "SC" ? 42 : imo.principaluf === "RJ" ? 33 : "",
                          nome: imo.principaluf, 
                          value: imo.principaluf === "RS" ? 43 : imo.principaluf === "SC" ? 42 : imo.principaluf === "RJ" ? 33 : ""
                      },
                      CEP: cidade.cep,
                      excluido: imo.principalsituacao === "VENDIDO" || imo.principalsituacao === "INATIVO" ? true : false,
                      desc_interna: imo.principaldescricao,
                      lat: imo.locallatitude,
                      long: imo.locallongitude,
                      created_at: new Date(imo.captacaocadem),
                      edited_at: new Date(imo.captacaoatuaem),
                      IPTU: imo.captacaovaliptu,
                      preço_condominio: imo.captacaovalcondominio,
                      descrição: imo.internetanunciointernet || imo.principaldescricao,
                      inativo: imo.internetpubsite === "1" ? false : true,
                      destaque: imo.internetpubdestaque === "1" ? true : false,
                      area_privativa: imo.detalheareaprivativa,
                      fotos: results                
                  }
                  let imoveis = await admin.firestore().doc(`empresas/ukMiO7mOb4NdCyEpoSV6/imoveis/${imo.fkimovel}`).set(template);
                  console.log("inseriu: imo.fkimovel")
                  
              // }
            })
            
            
                  // let imoveis = await Firebase.firestore().doc(`empresas/ukMiO7mOb4NdCyEpoSV6/imoveis/${imo.fkimovel}`).set(template);
                  
          },2000)
        }
        i++;
        //   
        // }
      })
      res.json({result: imgs});
    // Push the new message into Cloud Firestore using the Firebase Admin SDK.
    // Send back a message that we've succesfully written the message
    
  });
